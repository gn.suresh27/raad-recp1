resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.name}-gsb-static-data"
  acl           = "public-read"
  force_destroy = true
}